var fn = () => "response";


//TYPE DECLORATIONS: Premitive datatypes in javascript
var a: number;
var b: boolean;
var c: string;
var foo = undefined;

a = 10;
b = true;
c = "uma";

// ARRAY DECLORATIONS
var myArray: number[];
myArray = [];
myArray = [1,2,3];
myArray.push(4);
myArray.pop();
// myArray = ["uma","Mahesh"];    erro: Can't assign strign values to integer array.
// myArray.push("");    error: Can't push a string to integer array.
// b = myArray[1];    error: Can't assign integer array value to boolean. 

// tuple: declare array to accept multipiple datatypes as it's elements.
var multiArr: [number, boolean] = [1, true];
multiArr = [100, false]
// multiArr = [1]    error: need to declare all elements which are in tuple declaration


// FUNCTION DECLORATIONS
function add(a, b){
    return a+b;
}

var sum = add(1,2);
//var sum1 = add(1) ; var sum2 = add(1,2,3);     Error: Intypescript the number of arguments should be with its signature.
console.log(sum);

function sub(a: number, b: number, c=0, d?: number, e?: any) : number{
    return a + b + c;
}

var subtraction = sub(1,2);
console.log(subtraction)

// IMPLICIT DECLORATIONS
var x = 10;
var y = true;
var z;

// x = true;    Error: boolean is not assignable to number which is implicitly declared.
// y = 10;    Error: number is not assignable to boolean which is implicitly declared.
z = 10; z = true; z = "Universe";    // Works: Assignment is not in the declaration line, which makes it any type.

function greet(){
    return "Good morning";
}

var greeting = greet();

// greeting = 10;    Error: number is not assignable to string. Variable declaraiton & assignment are in the same line

var greetingAll;
greetingAll = greet();

greetingAll = 10  // works: it is declared and assigned in different lines.

//Union

var obj: number|boolean;

obj = 10 ; obj = true;
// obj = "test";     Error: obj is union of number & boolean datatypes, hence it don't accept any other datatypes.
