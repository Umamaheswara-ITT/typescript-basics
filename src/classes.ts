class Person{
    firstName;
    lastName: string;        // Member variables can be explicitly typed.

    // constructor(){
    //     this.firstName = " ";
    //     this.lastName = " ";
    // }

    constructor(firstName: string, lastName: string){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(){
        return this.firstName + this.lastName;
    }
}

var aPerson = new Person("uma", "Mahesh");        // Implicitly aPerson will be of type Person.
//var anotherPerson: Person = new Person();  // Explicitly declated it's type.
// aPerson.firstName = "Uma";
// aPerson.lastName = "Maheswar"
console.log(aPerson.firstName);
console.log(aPerson);

console.log(aPerson.getFullName());

let someObject = {
    firstName: "Uma",
    lastName: "Mahesh",
    addtionalVariable: "anything",
    getFullName: () => "Uma Mahesh"
}