class ThePerson{
    firstName: string;
    lastName: string;
    greet(){
        console.log("Hello!")
    }
}

class Programmer extends ThePerson{
    greet(){
        console.log("Hello world!");
    }
    greetLikeNormalPeople(){
        super.greet()
    
    }
}

var aProgrammer = new Programmer();

aProgrammer.greet();
aProgrammer.greetLikeNormalPeople();

//POLYMORPHISM
var anotherProgrammer: ThePerson = new Programmer();    //It can only use variables and methods of thePerson class
anotherProgrammer.greet();