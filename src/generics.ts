function echo(args){
    return args;
}

echo(1);
echo("Hi");

//-------------------------------------------
function echo_any(args: any): any{
    return args
}

var myStr: string = echo_any(1);

//-------------------------------------------
function echo_Gen<T>(args: T): T{
    return args;
}

var myNumber: number = echo_Gen(4);
// var aString: string = echo_Gen(6);    ERROR: we can not assaign cross datatypes for generic function calls.
//-------------------------------------------

class Human{
    firstName;
    lastName: string;

    constructor(firstName: string, lastName: string){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(){
        return this.firstName + this.lastName;
    }
}

class Admin extends Human{
}

class ProjectManager extends Human{
}

let admin = new Admin("Uma", "Mahesh");
let projectManager = new ProjectManager("Santinath","Mishra");

function personCategory(person){
    return person
}
var personDesignation = personCategory(admin);    // personDesignation is of type any




function personCategoryGen<T>(person: T): T{
    return person
}
var personDesignation1 = personCategoryGen(admin);    // personDesignation is of type Any - Because of generic method



function personEcho<T extends Person>(person: T): T{
    var firstName = person.firstName;
    var lastName = person.lastName
    return person
}

var personDesignation3 = personEcho(admin);
var personDesignation4 = personEcho(projectManager);
