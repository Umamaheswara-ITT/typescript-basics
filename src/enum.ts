enum DaysOfTheWeek{
    SUN, MON, TUE, WED, THU, FRI, SAT
}

let aDay: DaysOfTheWeek;

aDay = DaysOfTheWeek.WED;
if(aDay == DaysOfTheWeek.WED){
    console.log("Reminder: Kount Checkpoint meeting");
}

enum Cars {
    Tesla = 1,        // Can also only be first value initialized
    Ford,
    Audi,
    Mercedes
  }

  console.log(Cars.Tesla)
  console.log(Cars["Ford"])        // Reverse Mapping
  console.log(Cars[2])