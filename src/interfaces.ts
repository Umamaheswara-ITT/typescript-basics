//INTERFACES

interface Person{
    firstName: string;
    lastName: string;
    getFullName(): string;
}

class Men implements Person{
    firstName = "Uma";
    lastName = "Mahesh";
    getFullName(): string{
        return this.firstName + this.lastName;
    }
}

//DUCK TYPING: if it quacks like a duck, then it is a duck :)

let aPerson: Person = new Men();

let someObject = {
    firstName: "Uma",
    lastName: "Mahesh",
    addtionalVariable: "anything",
    getFullName: () => "Uma Mahesh"
}

console.log(aPerson.getFullName())

aPerson = someObject;   // it can be assigned to obj type person, eventhough it is not of the same type.
console.log(aPerson.firstName);
console.log(aPerson.getFullName());
// console.log(aPerson.additionalVariable);    Error: it can't be accessable through aPerson obj. it can with someObject