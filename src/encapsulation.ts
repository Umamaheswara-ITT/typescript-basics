class ThePerson{
    // private firstName: string;
    // private lastName: string;
    constructor(private firstName: string, private lastName: string){  //Automatially create member variables too
    }
    greet(){
        console.log("Hello!");
    }
    SetFirstName(firstName: string){
        this.firstName = firstName;
    }
    SetLastName(lastName: string){
        this.lastName = lastName;
    }
    GetFirstName(): string{
        return this.firstName;
    }
    GetLastName(): string{
        return this.lastName;
    }
    GetFullname(): string{
        return this.firstName + this.lastName;
    }

}

class Programmer extends ThePerson{
    greet(){
        console.log("Hello world!");
    }
    greetLikeNormalPeople(){
        super.greet()
    
    }
}

var aProgrammer = new Programmer("Uma","Mahesh");

aProgrammer.greet();
aProgrammer.greetLikeNormalPeople();

//POLYMORPHISM
// var anotherProgrammer: ThePerson = new Programmer("Uma","Universal");    //It can only use variables and methods of thePerson class
// anotherProgrammer.greet();

console.log(aProgrammer.GetFullname())