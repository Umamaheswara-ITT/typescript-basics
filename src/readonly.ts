class Car{
    readonly name: string;
    readonly model: string = "Model 3";
    constructor(name: string){    //Read only member variable can be initialized inline or in the constructor
        this.name = name;
    }
}

var aCar = new Car("Tesla");
console.log(aCar.name);

// aCar.name = ""        Error: can't assign a value to read only member variable